import socket
from datetime import datetime
import paho.mqtt.client as mqtt
from playhouse.shortcuts import model_to_dict
from database import models as md
from configuration.config import confi
import sys,json,uuid,time
import peewee as pw
from logs.logmanager import log
import os
from termcolor import colored

class database():
    def __init__(self):
        try:
            self.LOG = log()
            self.config = confi()
            self.localdb = pw.SqliteDatabase(self.config.DATABASE_PATH)
            md.proxy.initialize(self.localdb)
            md.devicedata.create_table(True)
            self.connection_flag = False
            self.a =0
            self.data = md.devicedata()
            self.MQTT_client = mqtt.Client()
            self.MQTT_Connect()

        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED TO INIT DB" + str(os.path.basename(__file__)) + str(e))  # error logs
                print(colored("EXCEPTION IN INIT DB - " + str(e),"red"))
                pass

    def MQTT_Connect(self):
        try:
            self.MQTT_client.username_pw_set("sachinrana2k18", "admin12345a")
            self.MQTT_client.connect("ec2-35-168-210-54.compute-1.amazonaws.com", 1883)
            self.a =1
            print(colored("SUCCESFULLY CONNECTED TO MQTT BROKER","blue"))
        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO MQTT SERVER CHECK CONNECTION" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("FAILLED TO MQTT SERVER CHECK CONNECTION - " + str(e),"red"))
            pass


    def Save_In_DataBase(self,payload,nodeid,nodetype,sensortype,):
        try:
            self.data.id = uuid.uuid4()
            self.data.nodeid = nodeid
            self.data.gatewayid = self.config.GATEWAY_ID
            self.data.orgid = self.config.ORG_ID
            self.data.nodetype = nodetype
            self.data.sensortype = sensortype
            self.data.payload = payload
            self.data.datestamp = self.get_datetime()[0]
            self.data.timestamp = self.get_datetime()[1]
            self.data.appname = self.config.APP_NAME
            self.data.msgtype = self.config.MSG_TYPE
            self.data.save(force_insert=True)
        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO SAVE DATA IN DATABASE ,DATABASE ERROR" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("EXCEPTION IN SAVE DATA IN DATABSE CHECK DATABASE MODELS- " + str(e),"red"))
            pass

    def get_datetime(self):
        DateString = "%Y-%m-%d"
        TimeString = "%H:%M:%S"
        date = str(datetime.now().strftime(DateString))
        time = str(datetime.now().strftime(TimeString))
        return date, time

    def update_synced(self, msg_id):
        try:
            query = md.devicedata.update(synced=1).where((md.devicedata.id == msg_id))
            query.execute()
        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO UPDATE SYNCED DATA TO 0---1" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("EXCEPTION IN UPDATING SYNCED DATA- " + str(e),"red"))
            pass

    def send_data(self):
        if self.is_connected() == True:
            if self.a == 1:
                for data in md.devicedata().select().order_by(md.devicedata.datestamp.desc() and md.devicedata.timestamp.asc()).where(md.devicedata.synced == 0).limit(self.config.BATCH_SIZE):
                    try:
                        self.MQTT_client.publish(topic=self.config.TOPIC_SEND,payload=json.dumps(model_to_dict(data)),qos=self.config.MQTT_QOS)
                        print(colored("PUBLISHING  DATA TO MQTT-:" + str(model_to_dict(data)),"green"))
                        msg_id = data.id
                        #print(msg_id)
                        self.update_synced(msg_id)
                    except:
                        e = sys.exc_info()[0]
                        self.LOG.ERROR("FAILLED TO SEND DATA TO THE SERVER" + str(os.path.basename(__file__)) + str(e))  # error logs
                        print(colored("EXCEPTION IN SENDING DATA TO AWS SERVER - " + str(e),"red"))
                        continue
            if self.a ==0:
                print(colored("DISSCONNECTED WITH MQTT CLIENT RETRY IN FEW SEC","blue"))
                self.LOG.ERROR("DISSCONNECTED WITH MQTT CLIENT RETRY IN FEW SEC" + str(os.path.basename(__file__)))
                self.MQTT_Connect()
                time.sleep(self.config.RETRY_TIME)

        if self.is_connected() == False:
            self.a=0
            print(colored("DISSCONNECTED WITH MQTT CLIENT RETRY IN FEW SEC","blue"))
            self.LOG.ERROR("DISSCONNECTED WITH MQTT CLIENT RETRY IN FEW SEC" + str(os.path.basename(__file__)))
            self.MQTT_Connect()
            time.sleep(self.config.RETRY_TIME)


    def is_connected(self):
        try:
            # connect to the host -- tells us if the host is actually
            # reachable
            socket.create_connection((self.config.PING_SERVER, self.config.SERVER_PORT))
            return True
        except OSError:
            pass
        return False
