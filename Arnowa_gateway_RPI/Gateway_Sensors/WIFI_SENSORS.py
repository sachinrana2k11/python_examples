import json
import os
import sys
import time
from queue import Queue
import paho.mqtt.client as mqtt
class Wifi:
    def __init__(self,q1):
        self.q1 = q1
        self.Wifi_client = mqtt.Client()
        self.connect_wifi_sensors()

    def connect_wifi_sensors(self):
        try:
            self.Wifi_client.username_pw_set("yatinbaluja", "raspberry")
            self.Wifi_client.connect("10.3.141.1", 1883)
            self.Wifi_client.on_connect = self.on_connect_WIFI
            self.Wifi_client.on_message = self.on_message_WIFI
            self.Wifi_client.loop_start()
            print("connected to rpi")

        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO WIFI SENSOR NEWORK CHECK CONNECTION" + str(os.path.basename(__file__)) + str(e))  # error logs
            print("FAILLED TO MQTT SERVER CHECK CONNECTION - " + str(e))
            pass


    def on_connect_WIFI(self,client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.Wifi_client.subscribe("ARNOWA/IN")

    def on_message_WIFI(self,client, userdata, msg):
        print(msg.payload.decode())
        payload = msg.payload.decode()
        #print(payload)
        self.q1.put(payload)

    def getdata(self,three,q1):
        if self.q1.empty() == False:
            temp_data = self.q1.get()
            #print(type(temp_data))
            if len(str(temp_data)) >50 :
                json_data = json.loads(temp_data)
                payload = json_data['Data']
                nodeid = json_data['DeviceID']
                nodetype = "WIFI"
                sensortype = json_data['SenosrType']
                return payload,nodeid,nodetype,sensortype


# wifi = Wifi()
#
# while 1:
#     print(wifi.getdata())
#     time.sleep(1)
