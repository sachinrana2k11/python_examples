from manager.handler import database
from time import  sleep
from datetime import datetime
from configuration.config import confi
from Gateway_Sensors.WIFI_SENSORS import Wifi
from Gateway_Sensors.MODBUS import MODBUS
from logs.logmanager import log
import multiprocessing
import Gateway_Sensors.BLE_SENSORS as BLE
from termcolor import colored
import os
LOG = log()
config = confi()
Data_base = database()
MOD = MODBUS()
q1 = multiprocessing.Queue(100)
wifi = Wifi(q1)
#ble = BLE()

def Get_Wifi_Sensors_data(three,q1):
    LOG.DEBUG("STARTING WIFI SENSORS-: "+str(os.path.basename(__file__)))
    while True:
        #wifi.connect_wifi_sensors()
        data = wifi.getdata(three,q1)
        if data != None:
            payload = data[0]
            nodeid = data[1]
            nodetype =data[2]
            sensortype = data[3]
            Data_base.Save_In_DataBase(payload,nodeid,nodetype,sensortype)
            print("PAYLOAD SAVE IN DATABSE IS :- "+str(payload))
            sleep(0.1)

def Put_Ble_Sensors_data(one,q):
    LOG.DEBUG("STARTING BLE SENSORS-: "+str(os.path.basename(__file__)))
    BLE.get_BLE(one,q)

def Get_Ble_Sensors_data(two,q):
    while True:
        data = BLE.get_Data(two,q)
        if data != None:
            payload = data
            nodeid = "1"
            nodetype ="BLE"
            sensortype = "DHT"
            Data_base.Save_In_DataBase(payload,nodeid,nodetype,sensortype)
            print("PAYLOAD SAVE IN DATABSE IS :- "+str(payload))
            #print(data)
            sleep(1)


def Get_Modbus_Sensors_data():
    LOG.DEBUG("STARTING MODBUS SENSORS-: " + str(os.path.basename(__file__)))
    while True:
        payload = MOD.get_data()
        Data_base.Save_In_DataBase(payload,"Modbus12","Serial","Modbus")
        print("PAYLOAD SAVE IN DATABSE IS :- " + str(payload))
        sleep(1)
def send_to_cloud():
    while True:
        Data_base.send_data()
        sleep(1)

if __name__ == '__main__':
    q = multiprocessing.Queue(100)
    
    p1 = multiprocessing.Process(target=Get_Modbus_Sensors_data)
    p2 = multiprocessing.Process(target=send_to_cloud)
    p3 = multiprocessing.Process(target=Put_Ble_Sensors_data, args=(1, q))
    p4 = multiprocessing.Process(target=Get_Ble_Sensors_data, args=(2, q))
    p5 = multiprocessing.Process(target=Get_Wifi_Sensors_data,args=(3,q1))                             
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    
    #Get_Wifi_Sensors_data()


