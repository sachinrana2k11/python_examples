#start from here sachin rana
import random

import ibmiotf.device,time,json
import paho.mqtt.client as mqtt
import minimalmodbus
import multiprocessing
port_modbus = 'COM11'
voltage = 0
current = 0
power = 0
frequency = 0

BLE_DHT_TEMP = 0
BLE_DHT_HUM = 0

param = {"org": "3n4830",
         "type": "NODE",
         "id": "MODBUS2k18",
         "auth-method": "token",
         "auth-token": "_hcXXOFbYdj+4Q)oZZ",
         "clean-session":"true"
         }
def get_BLE():
    global BLE_DHT_HUM,BLE_DHT_TEMP
    while 1:
        print("hello BLE")
        time.sleep(1)

def get_modbus_data():
    rs485 = minimalmodbus.Instrument(port_modbus, 1)
    rs485.serial.baudrate = 2400  # can be 9600, or another rate
    rs485.serial.bytesize = 8
    rs485.serial.parity = minimalmodbus.serial.PARITY_NONE
    rs485.serial.stopbits = 1
    rs485.serial.timeout = 1
    rs485.debug = False
    rs485.mode = minimalmodbus.MODE_RTU
    print(rs485)
    while 1:
        global voltage, current, power, frequency
        voltage = round(rs485.read_float(0, functioncode=4, numberOfRegisters=2),0)
        current = round(rs485.read_float(6, functioncode=4, numberOfRegisters=2),1)
        power = round(rs485.read_float(12, functioncode=4, numberOfRegisters=2),0)
        frequency = round(rs485.read_float(70, functioncode=4, numberOfRegisters=2),0)
        # form = {
        #     "voltage": random.randint(100,200), #round(Volts, 0),
        #     "current": random.randint(1,5), #round(Current, 1),
        #     "power":random.randint(100,1000), #round(Active_Power, 2),
        #     "frequency":random.randint(50,60) #round(Frequency, 0)
        # }
        #send_data_IBM(form)
        # voltage= random.randint(100,200)
        # current = random.randint(1,5)
        # power = random.randint(100,1000)
        # frequency = random.randint(50,60)
        time.sleep(2)

def send_data_IBM(payload):
        payload_final = json.loads(payload)
        payload_final['voltage'] = voltage
        payload_final['current'] = current
        payload_final['power'] = power
        payload_final['frequency'] = frequency
        payload_final['BLE_DHT_TEMP'] = BLE_DHT_TEMP
        payload_final['BLE_DHT_HUM'] = BLE_DHT_HUM
        #print(payload_final)
        temp = IBM_client.publishEvent("data", "json", payload_final, 0)
        print("data sending -:" + str(payload_final) + " " + str(temp))

def send_data_RPI(payload):
    ret = RPI_client.publish("arnowa/cmd", payload=json.dumps(payload), qos=0)

def send_ACK_IBM(msg):
    print(msg)
    if IBM_client.publishEvent("status", "json", json.dumps(msg), 2):
        print("ACK sended to cloud")
    else:
        print("Can't send ACK")

def myCommandCallback_IBM(cmd):
    print("Command received: %s" % cmd.data)
    send_ACK_IBM("data recived at gateway side")
    payload = cmd.data
    send_data_RPI(payload)

def on_connect_RPI(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("arnowa/data")

def on_message_RPI(client, userdata, msg):
        #print(msg.payload.decode())
        payload = msg.payload.decode()
        send_data_IBM(payload)


RPI_client = mqtt.Client()
RPI_client.username_pw_set("yatinbaluja","raspberry")
RPI_client.connect("10.3.141.1", 1883)
RPI_client.on_connect = on_connect_RPI
RPI_client.on_message = on_message_RPI
RPI_client.loop_start()
IBM_client = ibmiotf.device.Client(param)
IBM_client.connect()
IBM_client.commandCallback = myCommandCallback_IBM
if __name__ == '__main__':
    print("start the programm")
    q = Qur
    p1 = multiprocessing.Process(target=get_BLE())
    p1.start()
    p1.join
    get_modbus_data()
