#start from here sachin rana 
import re
import sys
import ibmiotf.device,time,json
import paho.mqtt.client as mqtt
import minimalmodbus
import serial
port_modbus = 'COM5'
param = {"org": "3n4830",
         "type": "NODE",
         "id": "MODBUS2k18",
         "auth-method": "token",
         "auth-token": "_hcXXOFbYdj+4Q)oZZ",
         "clean-session":"true"
         }
def send_data_IBM(payload):
    print(payload)
    payload["voltage":  round(Volts,0),
            "current": round(Current,1),
            "power": round(Active_Power,2),
            "frequency": round(Frequency,0)]
    temp = IBM_client.publishEvent("status", "json", payload, 0)
    print("data sending -:" + str(payload) + " " + str(temp))

def send_data_RPI(payload):
    ret = RPI_client.publish("arnowa/cmd", payload=json.dumps(payload), qos=0)

def send_ACK_IBM(msg):
    print(msg)
    if IBM_client.publishEvent("status", "json", json.dumps(msg), 2):
        print("ACK sended to cloud")
    else:
        print("Can't send ACK")

def myCommandCallback_IBM(cmd):
    print("Command received: %s" % cmd.data)
    send_ACK_IBM("data recived at gateway side")
    payload = cmd.data
    send_data_RPI(payload)

def on_connect_RPI(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("arnowa/data")

def on_message_RPI(client, userdata, msg):
        print(msg.payload.decode())
        payload = msg.payload.decode()
        send_data_IBM(payload)

RPI_client = mqtt.Client()
RPI_client.username_pw_set("yatinbaluja","raspberry")
RPI_client.connect("10.3.141.1", 1883)
RPI_client.on_connect = on_connect_RPI
RPI_client.on_message = on_message_RPI
RPI_client.loop_start()

IBM_client = ibmiotf.device.Client(param)
IBM_client.connect()
IBM_client.commandCallback = myCommandCallback_IBM
rs485 = minimalmodbus.Instrument(port_modbus, 1)
rs485.serial.baudrate = 2400  # can be 9600, or another rate
rs485.serial.bytesize = 8
rs485.serial.parity = minimalmodbus.serial.PARITY_NONE
rs485.serial.stopbits = 1
rs485.serial.timeout = 1
rs485.debug = False
rs485.mode = minimalmodbus.MODE_RTU
print(rs485)
if __name__ == '__main__':
    print("start the programm")
    while 1:
        time.sleep(1);