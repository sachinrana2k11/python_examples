import json
import sys
import time

from serial import Serial
from termcolor import colored

from configuration.config import confi
from logs.loghandler.logmanager import log

class LoRa:
    def init_port(self):
        self.LOG = log()  # init logs
        self.config = confi()  # init config
        self.ser = Serial(self.config.COM, self.config.BAUD)
        print("init sytart")

    def getdata(self):
        try:
            temp_data = self.ser.readline().decode()
            #print(temp_data)
            if len(temp_data) > self.config.MSG_LEN:
                json_data = json.loads(temp_data)
                payload = json_data['Data']
                nodeid = json_data['DeviceID']
                nodetype = "LoRa"
                sensortype = json_data['SenosrType']
                #self.ser.close()
                return payload, nodeid, nodetype, sensortype
            #self.ser.close()
            return "null"
        except:
            e = sys.exc_info()[0]
            #self.LOG.ERROR("FAILLED TO SAVE DATA IN DATABASE ,DATABASE ERROR" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("EXCEPTION IN Serial read - " + str(e), "red"))
            pass

# l = LoRa()
# while 1:
#     print(l.getdata())


