import configparser,os
from logs.loghandler.logmanager import log
config = configparser.ConfigParser()
config.read('configuration\\config.ini')

class confi:
    def __init__(self):
        self.LOG = log()
        self.MQTT_URL = str(config['MQTT']['MQTT_URL'])
        self.MQTT_PORT = int(config['MQTT']['MQTT_PORT'])
        self.MQTT_USR = str(config['MQTT']['MQTT_USR'])
        self.MQTT_PASS = str(config['MQTT']['MQTT_PASS'])
        self.MQTT_QOS = int(config['MQTT']['MQTT_QOS'])
        self.TOPIC_SEND = str(config['MQTT']['TOPIC_SEND'])
 # -------------------------database------------------------------------------------
        self.DATABASE_PATH= str(config['DATABASE']['DATABASE_PATH'])
        self.SYNC_TIME = int(config['DATABASE']['SYNC_TIME'])
        self.BATCH_SIZE = int(config['DATABASE']['BATCH_SIZE'])
        self.DEBUG_MODE = int(config['DATABASE']['DEBUG_MODE'])

        #--------------------------Device--------------------------------------------------
        self.GATEWAY_ID = str(config['DEVICE']['GATEWAY_ID'])
        self.ORG_ID = str(config['DEVICE']['ORG_ID'])
        self.APP_NAME = str(config['DEVICE']['APP_NAME'])
        self.RETRY_TIME = int(config['DEVICE']['RETRY_TIME'])
        self.MSG_TYPE = str(config['DEVICE']['MSG_TYPE'])
        self.CONNECTION_TYPE = str(config['DEVICE']['CONNECTION_TYPE'])
        self.PING_SERVER = str(config['DEVICE']['PING_SERVER'])
        self.SERVER_PORT = int(config['DEVICE']['SERVER_PORT'])
        self.LOG.DEBUG("Config file loaded : " + str(os.path.basename(__file__)))

        #------------------------Bucket S3 -----------------------------------------
        self.ACCESS_KEY_ID = str(config['AWS_S3']['ACCESS_KEY_ID'])
        self.ACCESS_SECRET_KEY = str(config['AWS_S3']['ACCESS_SECRET_KEY'])
        self.BUCKET_NAME = str(config['AWS_S3']['BUCKET_NAME'])
        self.FILE_NAME = str(config['AWS_S3']['FILE_NAME'])

        #-------------------------LoRa-Sensors-Config------------------------------
        self.COM = str(config['LORA_SENSORS']['COM'])
        self.BAUD = int(config['LORA_SENSORS']['BAUD'])
        self.MSG_LEN = int(config['LORA_SENSORS']['MSG_LEN'])

