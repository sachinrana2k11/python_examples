import json
import os
import sys
import time
from queue import Queue
import paho.mqtt.client as mqtt
class Wifi:
    def __init__(self):
        self.topic = "BLE_SCAN";
        #self.persons_list = []
        self.q = Queue()
        self.Wifi_client = mqtt.Client()
        self.connect_wifi_sensors()
    def connect_wifi_sensors(self):
        try:
            #self.Wifi_client.username_pw_set("yatinbaluja", "raspberry")
            self.Wifi_client.connect("192.168.43.39", 1883)
            self.Wifi_client.on_connect = self.on_connect_WIFI
            self.Wifi_client.on_message = self.on_message_WIFI
            self.Wifi_client.loop_start()

        except:
            e = sys.exc_info()[0]
            #self.LOG.ERROR("FAILLED TO WIFI SENSOR NEWORK CHECK CONNECTION" + str(os.path.basename(__file__)) + str(e))  # error logs
            print("FAILLED TO MQTT SERVER CHECK CONNECTION - " + str(e))
            pass


    def on_connect_WIFI(self,client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.Wifi_client.subscribe(self.topic)

    def on_message_WIFI(self,client, userdata, msg):
        # print(msg.payload.decodse())
        payload = msg.payload.decode()
        #print(payload)
        self.q.put(payload)

    def getdata(self):
        persons_list = []
        name_list = []
        if self.q.empty() == False:
            temp_data = self.q.get()
            #print(type(temp_data))
            if len(str(temp_data)) >10 :
                json_data = json.loads(temp_data)
                #print(json_data)
                li = list(json_data)
                for x in range(len(li)):
                    #print(li[x])
                    raw_data = li[x]
                    if int(raw_data["R"]) < 61:
                       #print(raw_data["id"])
                       persons_list.append(li[x])
                if len(persons_list)> 1:
                    print("Persons found in room " + raw_data["id"] + " " + str(len(persons_list)))
                for x in range(len(persons_list)):
                    name = persons_list[x]
                    name_list.append(name["M"])
                #print(name_list)
                if len(name_list) > 0:
                    print(name_list)



wifi = Wifi()

while 1:
    wifi.getdata()
    time.sleep(1)