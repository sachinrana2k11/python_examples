import json
import os
import sys
import time
import paho.mqtt.client as mqtt
import Rules.simulator_data as data
class Wifi:
    def __init__(self):
        self.topic = "BLE_SCAN";
        self.Wifi_client = mqtt.Client()
        self.connect_wifi_sensors()
    def connect_wifi_sensors(self):
        try:
            #self.Wifi_client.username_pw_set("yatinbaluja", "raspberry")
            self.Wifi_client.connect("192.168.43.39", 1883)
            print("connected")
            self.Wifi_client.loop_start()

        except:
            e = sys.exc_info()[0]
            #self.LOG.ERROR("FAILLED TO WIFI SENSOR NEWORK CHECK CONNECTION" + str(os.path.basename(__file__)) + str(e))  # error logs
            print("FAILLED TO MQTT SERVER CHECK CONNECTION - " + str(e))
            pass
    def send_data(self):
        self.Wifi_client.publish(self.topic,json.dumps(data.room1_scan),0)
        time.sleep(0.5)
        self.Wifi_client.publish(self.topic,json.dumps(data.room2_scan), 0)
        time.sleep(0.5)
        self.Wifi_client.publish(self.topic, json.dumps(data.room3_scan), 0)
        time.sleep(0.5)



wifi = Wifi()

while 1:
    wifi.send_data()
    #time.sleep(1)