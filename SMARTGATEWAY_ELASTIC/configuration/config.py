import configparser,os
from logs.loghandler.logmanager import log
config = configparser.ConfigParser()
config.read('configuration\\config.ini')

class confi:
    def __init__(self):
        self.LOG = log()
        self.HTPP_URL = str(config['HTTP']['HTPP_URL'])
        self.REQ_TYPE = str(config['HTTP']['REQ_TYPE'])
 # -------------------------database------------------------------------------------
        self.DATABASE_PATH= str(config['DATABASE']['DATABASE_PATH'])
        self.SYNC_TIME = int(config['DATABASE']['SYNC_TIME'])
        self.BATCH_SIZE = int(config['DATABASE']['BATCH_SIZE'])
        self.DEBUG_MODE = int(config['DATABASE']['DEBUG_MODE'])

        #--------------------------Device--------------------------------------------------
        self.GATEWAY_ID = str(config['DEVICE']['GATEWAY_ID'])
        self.ORG_ID = str(config['DEVICE']['ORG_ID'])
        self.APP_NAME = str(config['DEVICE']['APP_NAME'])
        self.RETRY_TIME = int(config['DEVICE']['RETRY_TIME'])
        self.MSG_TYPE = str(config['DEVICE']['MSG_TYPE'])
        self.CONNECTION_TYPE = str(config['DEVICE']['CONNECTION_TYPE'])
        self.PING_SERVER = str(config['DEVICE']['PING_SERVER'])
        self.SERVER_PORT = int(config['DEVICE']['SERVER_PORT'])
        self.LOG.DEBUG("Config file loaded : " + str(os.path.basename(__file__)))

        #------------------------Bucket S3 -----------------------------------------
        self.ACCESS_KEY_ID = str(config['AWS_S3']['ACCESS_KEY_ID'])
        self.ACCESS_SECRET_KEY = str(config['AWS_S3']['ACCESS_SECRET_KEY'])
        self.BUCKET_NAME = str(config['AWS_S3']['BUCKET_NAME'])
        self.FILE_NAME = str(config['AWS_S3']['FILE_NAME'])

        #-------------------------LoRa-Sensors-Config------------------------------
        self.LORA_COM = str(config['LORA_SENSORS']['LORA_COM'])
        self.LORA_BAUD = int(config['LORA_SENSORS']['LORA_BAUD'])
        self.LORA_MSG_LEN = int(config['LORA_SENSORS']['LORA_MSG_LEN'])
        self.LORA_REFRESH = int(config['LORA_SENSORS']['LORA_REFRESH'])

        #-------------------------WIFI-sensors-Config------------------------------
        self.MQTT_ADD = str(config['WIFI_SENSORS']['MQTT_ADD'])
        self.USR = str(config['WIFI_SENSORS']['USR'])
        self.PASS = str(config['WIFI_SENSORS']['PASS'])
        self.PORT = int(config['WIFI_SENSORS']['PORT'])
        self.TOPIC = str(config['WIFI_SENSORS']['TOPIC'])
        self.WIFI_REFRESH = int(config['WIFI_SENSORS']['WIFI_REFRESH'])
        self.WIFI_MSG_LEN = int(config['WIFI_SENSORS']['WIFI_MSG_LEN'])
        #-------------------------MODBUS-sensors_config----------------------------
        self.COM = str(config['MODBUS_SENSORS']['COM'])
        self.BAUD = str(config['MODBUS_SENSORS']['BAUD'])
        self.SLAVE_ID = str(config['MODBUS_SENSORS']['SLAVE_ID'])
        self.MODBUS_REFRESH = int(config['MODBUS_SENSORS']['MODBUS_REFRESH'])

        #------------------------BLE-sensors_Config--------------------------------
        self.BLE_REFRESH = int(config['BLE_SENSORS']['BLE_REFRESH'])
        self.BLE_SCAN_TIME = int(config['BLE_SENSORS']['BLE_SCAN_TIME'])
