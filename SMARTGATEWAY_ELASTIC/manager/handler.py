from datetime import datetime
import demjson
import requests
from playhouse.shortcuts import model_to_dict
from database import models as md
from configuration.config import confi
import sys,json,uuid,time
import peewee as pw
from logs.loghandler.logmanager import log
import os
from termcolor import colored

class database():
    def __init__(self):
        try:
            self.LOG = log()
            self.config = confi()
            self.localdb = pw.SqliteDatabase(self.config.DATABASE_PATH)
            md.proxy.initialize(self.localdb)
            md.devicedata.create_table(True)
            self.data = md.devicedata()

        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED TO INIT DB" + str(os.path.basename(__file__)) + str(e))  # error logs
                print(colored("EXCEPTION IN INIT DB - " + str(e),"red"))
                pass


    def Save_In_DataBase(self,payload,nodeid,nodetype,sensortype,):
        try:
            self.data.id = uuid.uuid4()
            self.data.nodeid = nodeid
            self.data.gatewayid = self.config.GATEWAY_ID
            self.data.orgid = self.config.ORG_ID
            self.data.nodetype = nodetype
            self.data.sensortype = sensortype
            self.data.payload = payload
            #self.data.datestamp = self.get_datetime()[0]
            self.data.timestamp = self.get_datetime()
            self.data.appname = self.config.APP_NAME
            self.data.msgtype = self.config.MSG_TYPE
            self.data.save(force_insert=True)
        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO SAVE DATA IN DATABASE ,DATABASE ERROR" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("EXCEPTION IN SAVE DATA IN DATABSE CHECK DATABASE MODELS- " + str(e),"red"))
            pass

    def get_datetime(self):
        # DateString = "%Y-%m-%d"
        # TimeString = "%H:%M:%S"
        # date = str(datetime.now().strftime(DateString))
        # time = str(datetime.now().strftime(TimeString))
        timestamp = str(datetime.now().utcnow().isoformat())
        return timestamp

    def update_synced(self, temp):
        try:
            #print(type(temp))
            for x in range(len(temp)):
                query = md.devicedata.update(synced=1).where((md.devicedata.id == temp[x]))
                query.execute()
                print("updated")
        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO UPDATE SYNCED DATA TO 0---1" + str(os.path.basename(__file__)) + str(e))  # error logs
            print(colored("EXCEPTION IN UPDATING SYNCED DATA- " + str(e),"red"))
            pass

    def send_data(self):
        try:
            #records = self.check_data_base()
            #print(records)
            # if records < 25:
            temp = []
            for data in md.devicedata().select().order_by(md.devicedata.timestamp.asc()).where(md.devicedata.synced == 0).limit(self.config.BATCH_SIZE):
                try:
                    data_raw = model_to_dict(data)
                    data_to_send = self.get_data_value(data_raw)
                    #print(data_to_send)
                    ack = self.send_data_elastic(data_to_send)
                    if ack == True:
                        #print("PUBLISHING  DATA TO CLOUD-:" + str(data_to_send))
                        msg_id = data.id
                        temp.append(msg_id)

                except:
                    e = sys.exc_info()[0]
                    self.LOG.ERROR("FAILLED TO SEND DATA TO THE SERVER" + str(os.path.basename(__file__)) + str(e))  # error logs
                    print("EXCEPTION IN SENDING DATA TO AWS SERVER - " + str(e))
                    continue
            self.update_synced(temp)

        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED TO SEND DATA TO AWS IOT" + str(os.path.basename(__file__)) + str(e))  # error logs
                print("EXCEPTION IN SENDING  DATA TO AWS - " + str(e))
                pass

    def get_data_value(self,data_raw):
        type = data_raw['sensortype']
        if type == 'Modbus':
            # Array of data objects
            raw_payload = json.loads(data_raw['payload'])

            dataArray = []
            for i in raw_payload:
                raw_form = {"meter": data_raw['nodeid'], "name": i, "value": raw_payload[i], "timestamp":data_raw['timestamp']}
                dataArray.append(raw_form)
            payload = ""

            # Creating request body
            for data in dataArray:
                # Pushing the operation and _id for each request
                type = {
                    "index": {
                        # "_id": data['id']
                    }
                }
                payload += demjson.encode(type) + "," + "\n" + demjson.encode(data) + "\n"
            return payload

        elif type == 'WIFI':
            # Array of data objects
            raw_payload = json.loads(data_raw['payload'])

            dataArray = []
            for i in raw_payload:
                raw_form = {"meter": data_raw['nodeid'], "name": i, "value": raw_payload[i],
                            "timestamp": str(datetime.now().utcnow().isoformat())}
                dataArray.append(raw_form)
            payload = ""

            # Creating request body
            for data in dataArray:
                # Pushing the operation and _id for each request
                type = {
                    "index": {
                        # "_id": data['id']
                    }
                }
                payload += demjson.encode(type) + "," + "\n" + demjson.encode(data) + "\n"
            return payload

        elif type == 'LORA':
            # Array of data objects
            raw_payload = json.loads(data_raw['payload'])

            dataArray = []
            for i in raw_payload:
                raw_form = {"meter": data_raw['nodeid'], "name": i, "value": raw_payload[i],
                            "timestamp": str(datetime.now().utcnow().isoformat())}
                dataArray.append(raw_form)
            payload = ""

            # Creating request body
            for data in dataArray:
                # Pushing the operation and _id for each request
                type = {
                    "index": {
                        # "_id": data['id']
                    }
                }
                payload += demjson.encode(type) + "," + "\n" + demjson.encode(data) + "\n"
            return payload

        elif type == 'BLE':
            # Array of data objects
            raw_payload = json.loads(data_raw['payload'])

            dataArray = []
            for i in raw_payload:
                raw_form = {"meter": data_raw['nodeid'], "name": i, "value": raw_payload[i],
                            "timestamp": str(datetime.now().utcnow().isoformat())}
                dataArray.append(raw_form)
            payload = ""

            # Creating request body
            for data in dataArray:
                # Pushing the operation and _id for each request
                type = {
                    "index": {
                        # "_id": data['id']
                    }
                }
                payload += demjson.encode(type) + "," + "\n" + demjson.encode(data) + "\n"
            return payload




    def send_data_elastic(self,payload):
        headers = {
            # 'Authorization': "Basic bWVxUmY4S0pDOjY1Y2MxNjFhLTIyYWQtNDBjNS1hYWFmLTVjMDgyZDVkY2ZkYQ==",
            'Content-Type': "application/x-ndjson"
        }

        # Bulk request including the index method and all data objects
        try:
            response = requests.request(self.config.REQ_TYPE,url=self.config.HTPP_URL, data=payload, headers=headers)
            #print(payload)
            parsed = json.loads(response.text)
            print(json.dumps(parsed, indent=4, sort_keys=True))
            print("data send successfully")
            return True

        except:
            e = sys.exc_info()[0]
            print("Exception in sending data " + str(e))
            return False
