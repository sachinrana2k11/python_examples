import os
import sys
import boto3
from botocore.client import Config
from datetime import datetime
from termcolor import colored
from configuration.config import confi
from logs.loghandler.logmanager import log

class DB_Upload:
    def __init__(self):
        try:
            self.config = confi()
            self.LOG = log()
            self.ACCESS_KEY_ID = self.config.ACCESS_KEY_ID
            self.ACCESS_SECRET_KEY = self.config.ACCESS_SECRET_KEY
            self.BUCKET_NAME = self.config.BUCKET_NAME
            self.FolderName = self.config.GATEWAY_ID
            self.data = open('database/data.db','rb')
            self.loc = "logs/"
            self.s3 = boto3.resource('s3',
                                     aws_access_key_id = self.ACCESS_KEY_ID,
                                     aws_secret_access_key = self.ACCESS_SECRET_KEY,
                                     config = Config(signature_version ='s3v4')
                                     )
        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED TO INIT UPLOAD DB" + str(os.path.basename(__file__)) + str(e))  # error logs
                print(colored("EXCEPTION IN INIT UPLOAD DB - " + str(e),"red"))
                pass

    def upload_file_db(self):
        try:
            #dtr = str(datetime.now())
            File_Name = self.FolderName + ('/')+ self.config.FILE_NAME
            self.s3.Bucket(self.BUCKET_NAME).put_object(Key = File_Name,Body = self.data)
            print("Database file uploaded succsfully")
            self.LOG.DEBUG("UPLOAD database on S3" + str(os.path.basename(__file__)))
            return True
        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED UPLOAD DB on S3" + str(os.path.basename(__file__)) + str(e))  # error logs
                print(colored("EXCEPTION UPLOAD DB on S3 - " + str(e),"red"))
                return False

    def files(self,path):
        for file in os.listdir(path):
            if os.path.isfile(os.path.join(path, file)):
                yield file

    def upload_files_logs(self):
        try:
            for file in self.files(self.loc):
                print(file)
                data = open(self.loc + file, 'rb')
                s3 = boto3.resource('s3',
                                    aws_access_key_id=self.ACCESS_KEY_ID,
                                    aws_secret_access_key=self.ACCESS_SECRET_KEY,
                                    config=Config(signature_version='s3v4')
                                    )
                s3.Bucket(self.BUCKET_NAME).put_object(Key=self.FolderName+("/")+self.loc + file, Body=data)
                self.LOG.DEBUG("UPLOAD Logs on S3" + str(os.path.basename(__file__)))  # Debug log
                print("logs uploded :-"+file)
            return True
        except:
                e = sys.exc_info()[0]
                self.LOG.ERROR("FAILLED UPLOAD DB on S3" + str(os.path.basename(__file__)) + str(e))  # error logs
                print(colored("EXCEPTION UPLOAD DB on S3 - " + str(e),"red"))
                return False