from time import gmtime, strftime, sleep
from queue import Queue
from bluepy.btle import Scanner, DefaultDelegate, BTLEException
from multiprocessing import Process,Queue
from configuration.config import confi
import sys
config = confi()
def get_BLE(one,q):
    class ScanDelegate(DefaultDelegate):
        def handleDiscovery(self, dev, isNewDev, isNewData):
            if "NODE-DHT" in str(dev.getScanData()):
                data = dev.getScanData()
                temp = str(data[2])
                temp1 = temp.split(',')
                #print(temp1)
                temprature = temp1[3]
                humidity =  temp1[4].replace("')",'')
                #print(temprature,humidity)
                data_t = {"temp":temprature,
                          "hum":humidity}
                # if q.full() == False:
                #     q.put(data_t)
                sys.stdout.flush()
                print(data_t)
                q.put(data_t)
    while 1:
        scanner = Scanner().withDelegate(ScanDelegate())
        scanner.scan(2, passive=True)
        sleep(config.BLE_SCAN_TIME)

def get_Data(two,q):
        if q.empty() == False:
            data =q.get()
            print(data)
            #sleep(1)




