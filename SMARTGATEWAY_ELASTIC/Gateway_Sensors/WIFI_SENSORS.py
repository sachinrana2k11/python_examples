import json
import os
import sys
import time
from queue import Queue
import paho.mqtt.client as mqtt
from configuration.config import confi
class Wifi:
    def __init__(self):
        self.q = Queue()
        self.config = confi()
        self.Wifi_client = mqtt.Client()
        self.connect_wifi_sensors()

    def connect_wifi_sensors(self):
        try:
            self.Wifi_client.username_pw_set(self.config.USR, self.config.PASS)
            self.Wifi_client.connect(self.config.MQTT_ADD, self.config.PORT)
            self.Wifi_client.on_connect = self.on_connect_WIFI
            self.Wifi_client.on_message = self.on_message_WIFI
            self.Wifi_client.loop_start()

        except:
            e = sys.exc_info()[0]
            self.LOG.ERROR("FAILLED TO WIFI SENSOR NEWORK CHECK CONNECTION" + str(os.path.basename(__file__)) + str(e))  # error logs
            print("FAILLED TO MQTT SERVER CHECK CONNECTION - " + str(e))
            pass


    def on_connect_WIFI(self,client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.Wifi_client.subscribe(self.config.TOPIC)

    def on_message_WIFI(self,client, userdata, msg):
        # print(msg.payload.decodse())
        payload = msg.payload.decode()
        #print(payload)
        self.q.put(payload)

    def getdata(self):
        if self.q.empty() == False:
            temp_data = self.q.get()
            #print(type(temp_data))
            if len(str(temp_data)) >self.config.WIFI_MSG_LEN:
                json_data = json.loads(temp_data)
                payload = json_data['Data']
                nodeid = json_data['DeviceID']
                nodetype = "WIFI"
                sensortype = json_data['SenosrType']
                return payload,nodeid,nodetype,sensortype


# wifi = Wifi()
#
# while 1:
#     print(wifi.getdata())
#     time.sleep(1)