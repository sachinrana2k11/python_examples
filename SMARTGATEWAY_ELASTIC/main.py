import sys
from termcolor import colored
from manager.handler import database
from time import  sleep
from configuration.config import confi
from Gateway_Sensors.MODBUS import MODBUS
from logs.loghandler.logmanager import log
import multiprocessing
from database.uploadDB import DB_Upload
from Gateway_Sensors.LORA_SENSORS import LoRa
from Gateway_Sensors.WIFI_SENSORS import Wifi
import Gateway_Sensors.BLE_SENSORS as BLE
import os

LOG = log()
config = confi()
Data_base = database()
Db_Up = DB_Upload()
MOD = MODBUS()
lora = LoRa()
wifi = Wifi()
ble = BLE()

def Get_Wifi_Sensors_data():
    LOG.DEBUG("STARTING WIFI SENSORS-: "+str(os.path.basename(__file__)))
    while True:
        #wifi.connect_wifi_sensors()
        data = wifi.getdata()
        if data != None:
            payload = data[0]
            nodeid = data[1]
            nodetype =data[2]
            sensortype = data[3]
            Data_base.Save_In_DataBase(payload,nodeid,nodetype,sensortype)
            print("PAYLOAD SAVE IN DATABSE IS :- "+str(payload))
            sleep(0.1)

def Put_Ble_Sensors_data(one,q):
    BLE.get_BLE(one,q)

def Get_Ble_Sensors_data(two,q):
    while True:
        data = BLE.get_Data(two,q)
        print(data)
        sleep(config.WIFI_REFRESH)


def Get_Modbus_Sensors_data():
    LOG.DEBUG("STARTING MODBUS SENSORS-: " + str(os.path.basename(__file__)))
    while True:
        payload = MOD.get_data()
        Data_base.Save_In_DataBase(payload,"Modbus12","Serial","Modbus")
        print("PAYLOAD SAVE IN DATABSE IS :- " + str(payload))
        sleep(config.MODBUS_REFRESH)

def get_data_lora():
    lora.init_port()
    while True:
         #wifi.connect_wifi_sensors()
         try:
             data = lora.getdata()
             #print(data)
             if data != "null":
                 payload = data[0]
                 nodeid = data[1]
                 nodetype =data[2]
                 sensortype = data[3]
                 Data_base.Save_In_DataBase(payload,nodeid,nodetype,sensortype)
                 print("PAYLOAD SAVE IN DATABSE IS :- "+str(payload))
                 sleep(config.LORA_REFRESH)
         except:
             e = sys.exc_info()[0]
             print(colored("EXCEPTION IN Save serial data to db - " + str(e), "red"))
             sleep(config.LORA_REFRESH)
             pass

def send_to_cloud():
    while True:
        Data_base.send_data()
        sleep(config.SYNC_TIME)

def upload_files():
    while 1:
        ack_db = Db_Up.upload_file_db()
        ack_logs = Db_Up.upload_files_logs()
        if ack_db == True and ack_logs == True:
            print("All files Uploaded Succesfully")
            sleep(3600)
        else:
            sleep(10)





if __name__ == '__main__':
    q = multiprocessing.Queue(100)
    p1 = multiprocessing.Process(target=Get_Modbus_Sensors_data)
    p2 = multiprocessing.Process(target=Get_Wifi_Sensors_data)
    p3 = multiprocessing.Process(target=send_to_cloud)
    p4 = multiprocessing.Process(target=Put_Ble_Sensors_data, args=(1, q))
    p5 = multiprocessing.Process(target=Get_Ble_Sensors_data, args=(2, q))
    p6 = multiprocessing.Process(target=upload_files)
    p7 = multiprocessing.Process(target=get_data_lora)
    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()

    #Get_Modbus_Sensors_data()
    #Get_Wifi_Sensors_data()
    #send_to_cloud()


