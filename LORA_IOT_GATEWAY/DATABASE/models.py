from peewee import *
proxy = Proxy()

class devicedata(Model):
    id = TextField(primary_key=True)
    nodeid = TextField()
    gatewayid = TextField()
    orgid = TextField()
    nodetype = TextField()
    sensortype = TextField()
    payload = TextField()
    datestamp = TextField()
    timestamp = TextField()
    appname = TextField()
    msgtype = TextField()
    synced = IntegerField(default=0)

    class Meta:
        database = proxy
