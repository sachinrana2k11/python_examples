import datetime
import uuid
from database import models as md
import peewee as pw
import time
from sensors.image_capture import camera
from sensors.Bar_code_reader import Reader
cam = camera()
reader = Reader()
localdb = pw.SqliteDatabase('database/data.db')
md.proxy.initialize(localdb)
md.devicedata.create_table(True)
data = md.devicedata()
def save_data(payload):
    DateString = "%Y-%m-%d"
    TimeString = "%H:%M:%S"
    data.id = uuid.uuid4()
    data.payload = payload
    data.deviceID = "test"
    data.datestamp = str(datetime.datetime.now().strftime(DateString))
    data.timestamp = str(datetime.datetime.now().strftime(TimeString))
    data.save(force_insert=True)
def main():
    while 1:
        data = reader.get_data()
        if data != None:
            cam.capture()
            save_data(data)
        time.sleep(5)

if __name__ == '__main__':
    main()