import os
import sys
import uuid
from datetime import datetime

import cv2
webcam = 0
USB_cam = 1
class camera:
    def __init__(self):
        try:
            self.DateString = "%Y-%m-%d"
            self.date = str(datetime.now().strftime(self.DateString))
            self.dir_name = str("images/" + self.date)
            if not os.path.exists(self.dir_name):
                os.makedirs(self.dir_name)
        except:
            e = sys.exc_info()[0]
            print("ERROR open camera "+str(e))

    def capture(self):
        self.camera = cv2.VideoCapture(webcam)
        return_value, image = self.camera.read()
        file_name = self.dir_name +'/'+str(uuid.uuid4())+'.png'
        #print(file_name)
        cv2.imwrite(file_name, image)
        del(self.camera)
        return True



