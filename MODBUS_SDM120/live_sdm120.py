import minimalmodbus
import paho.mqtt.client as mqttClient
import time
import json
connected = False  # Stores the connection status
BROKER_ENDPOINT = "industrial.api.ubidots.com"
TLS_PORT = 1883  # Secure port
MQTT_USERNAME = "BBFF-Jz7kMWMvJhErytGbKe7L4Mbghsga0k"  # Put here your Ubidots TOKEN
MQTT_PASSWORD = ""  # Leave this in blank
TOPIC = "/v1.6/devices/"
DEVICE_LABEL = "sdm120"
TLS_CERT_PATH = "industrial.cert"  # Put here the path of your TLS cert
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("[INFO] Connected to broker")
        global connected  # Use global variable
        connected = True  # Signal connection
    else:
        print("[INFO] Error, connection failed")
def on_publish(client, userdata, result):
    print("Published!")

rs485 = minimalmodbus.Instrument('/dev/ttyUSB0', 1)
rs485.serial.baudrate = 2400 # can be 9600, or another rate
rs485.serial.bytesize = 8
rs485.serial.parity = minimalmodbus.serial.PARITY_NONE
rs485.serial.stopbits = 1
rs485.serial.timeout = 1
rs485.debug = False
rs485.mode = minimalmodbus.MODE_RTU
print(rs485)
mqtt_client = mqttClient.Client()
mqtt_client.username_pw_set(MQTT_USERNAME, password=MQTT_PASSWORD)
mqtt_client.on_connect = on_connect
mqtt_client.on_publish = on_publish
# #mqtt_client.tls_set(ca_certs=TLS_CERT_PATH, certfile=None,
#                     keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
#                     tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
#mqtt_client.tls_insecure_set(True)
mqtt_client.connect(BROKER_ENDPOINT, port=TLS_PORT)
mqtt_client.loop_start()
topic = "{}{}".format(TOPIC, DEVICE_LABEL)
print(topic)


if __name__ == '__main__':
    while True:
        form = {
            "Volts":rs485.read_float(0, functioncode=4, numberOfRegisters=2),
            "Current": rs485.read_float(6, functioncode=4, numberOfRegisters=2),
            "Active_Power": rs485.read_float(12, functioncode=4, numberOfRegisters=2),
            "Apparent_Power": rs485.read_float(18, functioncode=4, numberOfRegisters=2),
            "Reactive_Power": rs485.read_float(24, functioncode=4, numberOfRegisters=2),
            "Power_Factor": rs485.read_float(30, functioncode=4, numberOfRegisters=2),
            "Phase_Angle": rs485.read_float(36, functioncode=4, numberOfRegisters=2),
            "Frequency": rs485.read_float(70, functioncode=4, numberOfRegisters=2),
            "Import_Active_Energy": rs485.read_float(72, functioncode=4, numberOfRegisters=2),
            "Export_Active_Energy": rs485.read_float(74, functioncode=4, numberOfRegisters=2),
            "Import_Reactive_Energy": rs485.read_float(76, functioncode=4, numberOfRegisters=2),
            "Export_Reactive_Energy": rs485.read_float(78, functioncode=4, numberOfRegisters=2),
            "Total_Active_Energy": rs485.read_float(342, functioncode=4, numberOfRegisters=2),
            "Total_Reactive_Energy": rs485.read_float(344, functioncode=4, numberOfRegisters=2)
        }  # make a data packet payload in dict object
        data = mqtt_client.publish(topic, json.dumps(form))
        print(data,form)
        time.sleep(5)


