import random

import paho.mqtt.client as mqttClient
import time
import json
connected = False  # Stores the connection status
BROKER_ENDPOINT = "industrial.api.ubidots.com"
TLS_PORT = 1883  # Secure port
MQTT_USERNAME = "BBFF-Jz7kMWMvJhErytGbKe7L4Mbghsga0k"  # Put here your Ubidots TOKEN
MQTT_PASSWORD = ""  # Leave this in blank
TOPIC = "/v1.6/devices/"
DEVICE_LABEL = "sdm120"
TLS_CERT_PATH = "industrial.cert"  # Put here the path of your TLS cert
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("[INFO] Connected to broker")
        global connected  # Use global variable
        connected = True  # Signal connection
    else:
        print("[INFO] Error, connection failed")
def on_publish(client, userdata, result):
    print("Published!")


mqtt_client = mqttClient.Client()
mqtt_client.username_pw_set(MQTT_USERNAME, password=MQTT_PASSWORD)
mqtt_client.on_connect = on_connect
mqtt_client.on_publish = on_publish
# #mqtt_client.tls_set(ca_certs=TLS_CERT_PATH, certfile=None,
#                     keyfile=None, cert_reqs=ssl.CERT_REQUIRED,
#                     tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
#mqtt_client.tls_insecure_set(True)
mqtt_client.connect(BROKER_ENDPOINT, port=TLS_PORT)
mqtt_client.loop_start()
topic = "{}{}".format(TOPIC, DEVICE_LABEL)
print(topic)


if __name__ == '__main__':
    while True:
        form = {
            "Volts": random.randint(220, 250),
            "Current": random.randint(0, 10),
            "Active_Power": random.randint(100, 500),
            "Apparent_Power": random.randint(100, 300),
            "Reactive_Power": random.randint(250, 300),
            "Power_Factor": random.randint(0, 1),
            "Phase_Angle": random.randint(0, 180),
            "Frequency": random.randint(50, 60),
            "Import_Active_Energy": random.randint(100, 250),
            "Export_Active_Energy": random.randint(100, 250),
            "Import_Reactive_Energy": random.randint(100, 250),
            "Export_Reactive_Energy": random.randint(100, 250),
            "Total_Active_Energy": random.randint(10, 250),
            "Total_Reactive_Energy": random.randint(10, 250)
        }  # make a data packet payload in dict object
        data = mqtt_client.publish(topic, json.dumps(form))
        print(data,form)
        time.sleep(5)


